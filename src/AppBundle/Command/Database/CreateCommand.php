<?php

namespace AppBundle\Command\Database;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:create-database')
            ->setDescription('Create all of the relevant database tables')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dbal = $this->getContainer()->get('database_connection');

        $dbal->query("DROP TABLE IF EXISTS transactions");
        $dbal->query("DROP TABLE IF EXISTS customers");

        $dbal->query("
            CREATE TABLE customers (
                id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                email VARCHAR(255) UNIQUE NOT NULL,
                first_name VARCHAR(255) NOT NULL,
                last_name VARCHAR(255) NOT NULL,
                gender VARCHAR(1) NOT NULL,
                country VARCHAR(2) NOT NULL,
                balance INT,
                balance_bonus INT,
                bonus_pct INT
            ) ENGINE=InnoDB;
        ");

        $dbal->query("
            CREATE TABLE transactions (
                id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                user_id INT UNSIGNED NOT NULL,
                event_date DATETIME,
                country VARCHAR(2),
                amount INT,
                bonus INT,
                FOREIGN KEY (user_id) REFERENCES customers(id) ON DELETE CASCADE
            ) ENGINE=InnoDB;
        ");

        $output->writeln(print_r($dbal->fetchAll('SHOW TABLES')));
    }
}